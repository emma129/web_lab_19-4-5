DROP TABLE IF EXISTS User_Info;

CREATE TABLE User_Info (
  username   VARCHAR(99) PRIMARY KEY,
  password   BLOB,
  salt       BLOB
);

