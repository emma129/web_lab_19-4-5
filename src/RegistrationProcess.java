

import DAO.LoginInfoDAO;
import Info.LoginInfo;
import database.MySQL;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

public class RegistrationProcess extends HttpServlet {
    private MySQL mySQL = new MySQL();
    private String username;
    private String password;



    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        HttpSession session = request.getSession(true);
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        if (((String) session.getAttribute("status")) == null) {
            session.setAttribute("status","logout");
        }

        if(((String) session.getAttribute("status")).equals("login")){
            response.sendRedirect("contentPage.jsp");
        }else{
            username = request.getParameter("username");
            if(username == null){
                session.setAttribute("RegisterMessage","");
                request.getRequestDispatcher("Registration.jsp").forward(request, response);
            }
            password = request.getParameter("password");
            byte[] salt= Passwords.getNextSalt();
            byte[] hashPassword = Passwords.hash(password.toCharArray(),salt,5);
            try {
                LoginInfoDAO.createLoginInfo(mySQL, username, hashPassword, salt);
                out.println("<h1>Register successfully!</h1><br><a href=\"Login.jsp\">Login</a>");
            }catch(SQLException e){
                session.setAttribute("RegisterMessage","Username already exist, please pick another one");
                request.getRequestDispatcher("Registration.jsp").forward(request, response);
            }
        }
    }
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        doPost(request,response);
    }
}
