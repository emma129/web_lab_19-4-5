
import DAO.LoginInfoDAO;
import Info.LoginInfo;
import database.AbstractDB;
import database.MySQL;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;


public class LoginProcess extends HttpServlet {
    private MySQL mySQL = new MySQL();
    private String username;
    private String password;


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        PrintWriter out= response.getWriter();

        HttpSession session = request.getSession(true);

        if (( session.getAttribute("status")) == null) {
            session.setAttribute("status","logout");
            request.getRequestDispatcher("Login.jsp").forward(request, response);
        }
        if ((session.getAttribute("status")).equals("login")) {
            response.sendRedirect("contentPage.jsp");
        } else {
            username = request.getParameter("username");
            password = request.getParameter("password");
            if (username == null){
                session.setAttribute("loginMessage", "");
                session.setAttribute("logoutMessage", "");
                request.getRequestDispatcher("Login.jsp").forward(request, response);
            }
            LoginInfo loginInfo = LoginInfoDAO.getLoginInfo(mySQL, username);

            if (loginInfo == null){
                session.setAttribute("loginMessage", "Fail to login: wrong username");
                request.getRequestDispatcher("Login.jsp").forward(request, response);
            }
            if (Passwords.isExpectedPassword(password.toCharArray(), loginInfo.getSalt(), 5, loginInfo.getPassword())) {
                session.setAttribute("loginMessage", "");
                session.setAttribute("status", "login");
                session.setAttribute("username",username);
               out.println("<h1>Register successfully!</h1> <br> <a href=\"Login.jsp\"></a>");
            } else {
                session.setAttribute("loginMessage", "Fail to login: wrong password");
                request.getRequestDispatcher("Login.jsp").forward(request, response);
            }
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        doPost(request,response);
    }

}
